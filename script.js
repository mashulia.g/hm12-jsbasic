let btnEnter = document.getElementById("btnEnter");
let btnS = document.getElementById("btnS");
let btnE = document.getElementById("btnE");
let btnO = document.getElementById("btnO");
let btnN = document.getElementById("btnN");
let btnL = document.getElementById("btnL");
let btnZ = document.getElementById("btnZ");

document.addEventListener("keydown", (event) => {
  if (event.code == "Enter") {
      btnEnter.style.backgroundColor = 'blue';
      btnS.style.backgroundColor = 'black';
      btnE.style.backgroundColor = 'black';
      btnO.style.backgroundColor = 'black';
      btnN.style.backgroundColor = 'black';
      btnL.style.backgroundColor = 'black';
      btnZ.style.backgroundColor = 'black';
  } else if (event.code == "KeyS") {
      btnEnter.style.backgroundColor = 'black';
      btnS.style.backgroundColor = 'blue';
      btnE.style.backgroundColor = 'black';
      btnO.style.backgroundColor = 'black';
      btnN.style.backgroundColor = 'black';
      btnL.style.backgroundColor = 'black';
      btnZ.style.backgroundColor = 'black';
  } else if (event.code == "KeyE") {
      btnEnter.style.backgroundColor = 'black';
      btnS.style.backgroundColor = 'black';
      btnE.style.backgroundColor = 'blue';
      btnO.style.backgroundColor = 'black';
      btnN.style.backgroundColor = 'black';
      btnL.style.backgroundColor = 'black';
      btnZ.style.backgroundColor = 'black';
  } else if (event.code == "KeyO") {
      btnEnter.style.backgroundColor = 'black';
      btnS.style.backgroundColor = 'black';
      btnE.style.backgroundColor = 'black';
      btnO.style.backgroundColor = 'blue';
      btnN.style.backgroundColor = 'black';
      btnL.style.backgroundColor = 'black';
      btnZ.style.backgroundColor = 'black';
  } else if (event.code == "KeyN") {
      btnEnter.style.backgroundColor = 'black';
      btnS.style.backgroundColor = 'black';
      btnE.style.backgroundColor = 'black';
      btnO.style.backgroundColor = 'black';
      btnN.style.backgroundColor = 'blue';
      btnL.style.backgroundColor = 'black';
      btnZ.style.backgroundColor = 'black';
  } else if (event.code == "KeyL") {
      btnEnter.style.backgroundColor = 'black';
      btnS.style.backgroundColor = 'black';
      btnE.style.backgroundColor = 'black';
      btnO.style.backgroundColor = 'black';
      btnN.style.backgroundColor = 'black';
      btnL.style.backgroundColor = 'blue';
      btnZ.style.backgroundColor = 'black';
  } else if (event.code == "KeyZ") {
      btnEnter.style.backgroundColor = 'black';
      btnS.style.backgroundColor = 'black';
      btnE.style.backgroundColor = 'black';
      btnO.style.backgroundColor = 'black';
      btnN.style.backgroundColor = 'black';
      btnL.style.backgroundColor = 'black';
      btnZ.style.backgroundColor = 'blue';
  }
});


